<?php

return array(
    // Core
	'salt' => 'au791ae620-e2f5-11db-8314-0800200c9a66-13245',
    'dateFormats' => array(
	    'short' => 'Y-m-d',
	    'long' => 'Y-m-d H:i:s',
	),
	'sizeLimit' => 2*1024*1024,
	'pageSize' => 10,
    'passwordLMin' => 6,
    'resetForGotPasswordTimeout' => 86400,
	'fileExtensions' => array(
		'image' => array('gif', 'png', 'jpg'),
	),
	// Projects
    
    'mailSender' => array(
        "name"     => "OurAuction",
        "host"     => "smtp.gmail.com",
        "port"     => 587, // local 465
        "secure"   => "tls", // local ssl
        "username" => "mailer@ceres.com.vn",
        "password" => "ceres.m@iler",
        "method"   => "smtp",
        "charset"  => "UTF-8",
        "from"     => "contact@example.com",
        "reply"    => "no-reply@dotgain.com.au",
    ),
);