<?php

Yii::setPathOfAlias('bootstrap', poa('common') . '/extensions/bootstrap');

return array(
    'preload'    => array('log'),
    'import'     => array(
        'common.models.*',
        'common.components.*',
        'common.controllers.*',
        // Application included paths must be set after common included paths.
        'app.models.*',
        'app.components.*',
    ),
    'components' => array(
        'clientScript' => array(
            'class' => 'GClientScript',
        ),
        'user'         => array(
            'class'          => 'GWebUser',
            'allowAutoLogin' => true,
        ),
        'urlManager'   => array(
            'urlFormat'      => 'path',
            'showScriptName' => false,
            'rules'          => array(
            )
        ),
        'db'           => array(
            'connectionString' => 'mysql:host=localhost;dbname=ourauctions',
            'emulatePrepare'   => true,
            'username'         => 'root',
            'password'         => '',
            'charset'          => 'utf8',
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'bootstrap'    => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        'log' => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    'params'     => require(dirname(__FILE__) . '/params.php'),
);