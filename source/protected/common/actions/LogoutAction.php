<?php

class LogoutAction extends CAction
{
	public function run()
	{
        user()->logout();
        $this->controller->redirect(app()->baseUrl);
	}
}