<?php

class ErrorAction extends CAction
{
	public function run()
	{
		if ($error = app()->errorHandler->error) {
			$this->controller->layout = null;
			$this->controller->render('error', array('error' => $error));
		}
	}
}