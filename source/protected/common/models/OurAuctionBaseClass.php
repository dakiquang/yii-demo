<?php

abstract class OurAuctionBaseClass extends CActiveRecord
{

    /**
     * Prepares attributes before performing validation.
     */
    protected function afterValidate()
    {

        if ($this->isNewRecord) {// create
            $this->created_time = $this->updated_time = time();
        } else {// update
            $this->updated_time = time();
        }
        return true;
    }

}
