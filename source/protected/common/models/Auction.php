<?php
/**
* This is the model class for table "auctions".
*
* The followings are the available columns in table 'auctions':
* @property integer $id
* @property integer $product_id
* @property integer $subdomain_id
* @property integer $created_by
* @property integer $auction_type
* @property string $starting_price
* @property string $current_price
* @property string $final_price
* @property integer $lastest_bidder_id
* @property string $expired_time
* @property integer $created_time
*/
class Auction extends CActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return Auction the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'auctions';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('product_id, subdomain_id, created_by, auction_type, current_price, expired_time, created_time', 'required'),
			array('product_id, subdomain_id, created_by, auction_type, lastest_bidder_id, created_time', 'numerical', 'integerOnly' => true),
			array('starting_price, current_price, final_price', 'length', 'max' => 10),
			array('id, product_id, subdomain_id, created_by, auction_type, starting_price, current_price, final_price, lastest_bidder_id, expired_time, created_time', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'subdomain_id' => 'Subdomain',
			'created_by' => 'Created By',
			'auction_type' => 'Auction Type',
			'starting_price' => 'Starting Price',
			'current_price' => 'Current Price',
			'final_price' => 'Final Price',
			'lastest_bidder_id' => 'Lastest Bidder',
			'expired_time' => 'Expired Time',
			'created_time' => 'Created Time',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('product_id', $this->product_id);
		$criteria->compare('subdomain_id', $this->subdomain_id);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('auction_type', $this->auction_type);
		$criteria->compare('starting_price', $this->starting_price, true);
		$criteria->compare('current_price', $this->current_price, true);
		$criteria->compare('final_price', $this->final_price, true);
		$criteria->compare('lastest_bidder_id', $this->lastest_bidder_id);
		$criteria->compare('expired_time', $this->expired_time, true);
		$criteria->compare('created_time', $this->created_time);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}