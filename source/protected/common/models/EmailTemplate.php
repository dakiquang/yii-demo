<?php
/**
* This is the model class for table "email_templates".
*
* The followings are the available columns in table 'email_templates':
* @property string $id
* @property string $name
* @property string $description
* @property string $sender_name
* @property string $sender_email
* @property string $receiver_name
* @property string $receiver_email
* @property string $subject
* @property string $body
*/
class EmailTemplate extends CActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return EmailTemplate the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'email_templates';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('name, sender_name, sender_email, receiver_name, receiver_email, subject, body', 'required'),
			array('name', 'length', 'max' => 128),
			array('sender_name, sender_email, receiver_name, receiver_email', 'length', 'max' => 255),
			array('description', 'safe'),
			array('id, name, description, sender_name, sender_email, receiver_name, receiver_email, subject, body', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'sender_name' => 'Sender Name',
			'sender_email' => 'Sender Email',
			'receiver_name' => 'Receiver Name',
			'receiver_email' => 'Receiver Email',
			'subject' => 'Subject',
			'body' => 'Body',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('sender_name', $this->sender_name, true);
		$criteria->compare('sender_email', $this->sender_email, true);
		$criteria->compare('receiver_name', $this->receiver_name, true);
		$criteria->compare('receiver_email', $this->receiver_email, true);
		$criteria->compare('subject', $this->subject, true);
		$criteria->compare('body', $this->body, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
    
    public static function getTemplate($name) 
    {
		return self::model()->findByAttributes(array('name' => $name));
	}
}