<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $company_name
 * @property integer $newsletter
 * @property string $logo
 * @property integer $status
 * @property string $activation_key
 * @property string $address
 * @property string $address_2
 * @property integer $state_id
 * @property integer $post_code
 * @property integer $created_time
 * @property integer $updated_time
 */
class User extends OurAuctionBaseClass
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public $password_repeat;

    const STATUS_ACTIVE   = 1;
    const STATUS_DEACTIVE = 0;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            // required
            array('email, password, state_id', 'required'),
            array('password_repeat', 'required', 'on' => 'register'),
            // lenght
            array('email, activation_key', 'length', 'max' => 128),
            array('phone', 'length', 'max' => 32),
            array('password', 'length', 'min' => param('passwordLMin'), 'max' => 32),
            array('first_name, last_name, company_name', 'length', 'max' => 64),
            array('address, address_2', 'length', 'max' => 100),
            array('post_code', 'length', 'min' => 4),
            // Validation
            array('email', 'email'),
            array('email', 'unique'),
            //array('password_repeat', 'passwordConfirm'),
            array('password_repeat', 'compare', 'compareAttribute' => 'password', 'on' => 'register'),
            array('newsletter, status, state_id, post_code, created_time, updated_time', 'numerical', 'integerOnly' => true),
            // Safe
            array('logo, password_repeat, passwordResetString, passwordResetTimeout', 'safe'),
            // search
            array('id, email, password, first_name, last_name, phone, company_name, newsletter, logo, status, activation_key, address, address_2, state_id, post_code, created_time, updated_time', 'safe', 'on' => 'search'),
        );
    }
    
    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->password = Utils::md5($this->password);
        }
        return true;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name => label)
     */
    public function attributeLabels()
    {
        return array(
            'id'              => 'ID',
            'email'           => 'Email',
            'password'        => 'Password',
            'passwordConfirm' => 'Confirm Password',
            'first_name'      => 'First Name',
            'last_name'       => 'Last Name',
            'phone'           => 'Phone',
            'company_name'    => 'Company Name',
            'newsletter'      => 'News Letter',
            'logo'            => 'Logo',
            'status'          => 'Status',
            'activation_key'  => 'Activation Key',
            'address'         => 'Address',
            'address_2'       => 'Address 2',
            'state_id'        => 'State',
            'post_code'       => 'Post Code',
            'created_time'    => 'Created Time',
            'updated_time'    => 'Updated Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('company_name', $this->company_name, true);
        $criteria->compare('newsletter', $this->newsletter);
        $criteria->compare('logo', $this->logo, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('activation_key', $this->activation_key, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('address_2', $this->address_2, true);
        $criteria->compare('state_id', $this->state_id);
        $criteria->compare('post_code', $this->post_code);
        $criteria->compare('created_time', $this->created_time);
        $criteria->compare('updated_time', $this->updated_time);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => array(
                'pageSize' => param('pageSize'),
            ),
        ));
    }

}