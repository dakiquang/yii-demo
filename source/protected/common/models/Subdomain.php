<?php
/**
* This is the model class for table "subdomain".
*
* The followings are the available columns in table 'subdomain':
* @property integer $id
* @property integer $user_id
* @property string $name
* @property string $address
* @property string $address_2
* @property string $company_name
* @property string $header_background
* @property string $logo
* @property integer $created_time
* @property integer $updated_time
*/
class Subdomain extends CActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return Subdomain the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'subdomain';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('user_id, name, created_time', 'required'),
			array('user_id, created_time, updated_time', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 60),
			array('address, address_2', 'length', 'max' => 100),
			array('company_name', 'length', 'max' => 64),
			array('header_background', 'length', 'max' => 20),
			array('logo', 'safe'),
			array('id, user_id, name, address, address_2, company_name, header_background, logo, created_time, updated_time', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'name' => 'Name',
			'address' => 'Address',
			'address_2' => 'Address 2',
			'company_name' => 'Company Name',
			'header_background' => 'Header Background',
			'logo' => 'Logo',
			'created_time' => 'Created Time',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('address_2', $this->address_2, true);
		$criteria->compare('company_name', $this->company_name, true);
		$criteria->compare('header_background', $this->header_background, true);
		$criteria->compare('logo', $this->logo, true);
		$criteria->compare('created_time', $this->created_time);
		$criteria->compare('updated_time', $this->updated_time);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}