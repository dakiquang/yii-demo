<?php
/**
* This is the model class for table "credit_current".
*
* The followings are the available columns in table 'credit_current':
* @property integer $id
* @property integer $user_id
* @property integer $credit
* @property integer $updated_time
*/
class CreditCurrent extends CActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return CreditCurrent the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'credit_current';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('user_id, updated_time', 'required'),
			array('user_id, credit, updated_time', 'numerical', 'integerOnly' => true),
			array('id, user_id, credit, updated_time', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'credit' => 'Credit',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('credit', $this->credit);
		$criteria->compare('updated_time', $this->updated_time);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}