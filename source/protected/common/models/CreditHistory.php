<?php
/**
* This is the model class for table "credit_histories".
*
* The followings are the available columns in table 'credit_histories':
* @property integer $id
* @property integer $user_id
* @property integer $number_credit
* @property string $price
* @property string $amount
* @property string $invoice
* @property integer $pay_time
*/
class CreditHistory extends CActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return CreditHistory the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'credit_histories';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('user_id, number_credit, price, amount, pay_time', 'required'),
			array('user_id, number_credit, pay_time', 'numerical', 'integerOnly' => true),
			array('price, amount', 'length', 'max' => 10),
			array('invoice', 'length', 'max' => 16),
			array('id, user_id, number_credit, price, amount, invoice, pay_time', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'number_credit' => 'Number Credit',
			'price' => 'Price',
			'amount' => 'Amount',
			'invoice' => 'Invoice',
			'pay_time' => 'Pay Time',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('number_credit', $this->number_credit);
		$criteria->compare('price', $this->price, true);
		$criteria->compare('amount', $this->amount, true);
		$criteria->compare('invoice', $this->invoice, true);
		$criteria->compare('pay_time', $this->pay_time);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}