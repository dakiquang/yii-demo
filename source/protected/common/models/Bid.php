<?php
/**
* This is the model class for table "bids".
*
* The followings are the available columns in table 'bids':
* @property integer $id
* @property integer $auction_id
* @property integer $bidder_id
* @property string $amount
* @property integer $updated_time
* @property integer $created_time
*/
class Bid extends CActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return Bid the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'bids';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('auction_id, bidder_id, amount, created_time', 'required'),
			array('auction_id, bidder_id, updated_time, created_time', 'numerical', 'integerOnly' => true),
			array('amount', 'length', 'max' => 10),
			array('id, auction_id, bidder_id, amount, updated_time, created_time', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'auction_id' => 'Auction',
			'bidder_id' => 'Bidder',
			'amount' => 'Amount',
			'updated_time' => 'Updated Time',
			'created_time' => 'Created Time',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('auction_id', $this->auction_id);
		$criteria->compare('bidder_id', $this->bidder_id);
		$criteria->compare('amount', $this->amount, true);
		$criteria->compare('updated_time', $this->updated_time);
		$criteria->compare('created_time', $this->created_time);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}