<?php

/**
 * This is the model class for table "emails".
 *
 * The followings are the available columns in table 'emails':
 * @property string $id
 * @property integer $type
 * @property string $sender_email
 * @property string $sender_name
 * @property string $recipient_email
 * @property string $recipient_name
 * @property string $cc
 * @property string $bcc
 * @property string $subject
 * @property string $content
 * @property integer $is_sent
 * @property integer $sent_time
 * @property integer $created_time
 */
class Email extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Email the static model class
     */
    
    const URGENT = 1;
	const NORMAL = 2;
	const DAILY = 3;
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'emails';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('sender_email, sender_name, subject', 'required'),
            array('type, is_sent, sent_time, created_time', 'numerical', 'integerOnly' => true),
            array('sender_email, sender_name, recipient_email, recipient_name, cc, bcc', 'length', 'max' => 255),
            array('content', 'safe'),
            array('id, type, sender_email, sender_name, recipient_email, recipient_name, cc, bcc, subject, content, is_sent, sent_time, created_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name => label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'sender_email' => 'Sender Email',
            'sender_name' => 'Sender Name',
            'recipient_email' => 'Recipient Email',
            'recipient_name' => 'Recipient Name',
            'cc' => 'Cc',
            'bcc' => 'Bcc',
            'subject' => 'Subject',
            'content' => 'Content',
            'is_sent' => 'Is Sent',
            'sent_time' => 'Sent Time',
            'created_time' => 'Created Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('sender_email', $this->sender_email, true);
        $criteria->compare('sender_name', $this->sender_name, true);
        $criteria->compare('recipient_email', $this->recipient_email, true);
        $criteria->compare('recipient_name', $this->recipient_name, true);
        $criteria->compare('cc', $this->cc, true);
        $criteria->compare('bcc', $this->bcc, true);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('is_sent', $this->is_sent);
        $criteria->compare('sent_time', $this->sent_time);
        $criteria->compare('created_time', $this->created_time);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => param('pageSize'),
                    ),
                ));
    }
    
    public function beforeSave() {
		if (empty($this->created_time))
			$this->created_time = time();
		return parent::beforeSave();
	}

    public function sendMail()
    {
        $from = array($this->sender_name, $this->sender_name);

        //build cc array
        $cc = $this->buildCcBcc($this->cc);
        //build bcc array
        $bcc = $this->buildCcBcc($this->bcc);

        //send email by condition
        if (isset($this->recipient_email) && !empty($this->recipient_email)) {
            $to = array($this->recipient_email, $this->recipient_name);
            //send mail
            $result = EmailHelper::queueSend($this->subject, $this->content, $from, $to, null, $cc, $bcc);
        } else {
            Yii::log(date("Y-m-d") . " Email - : There's no recipient", 'error');
            return false;
        }

        /*
          if ($result) { //send mail success
          $this->isSent = 1;
          $this->sentTime = time();
          $this->save();
          } else
          Yii::log("Send mail to " . $this->recipientEmail . " unsuccessful at " . date("Y-m-h H:i:s"), 'error');
         */

        if ($result) { //send mail success
            $this->is_sent = 1;
            $this->sent_time = time();
            $this->save();
            echo "Sendmail success \n\n";
        } else {
            Yii::log("Send mail to " . $this->recipient_email . " unsuccessful at " . date("Y-m-h H:i:s"), 'error');
            var_dump($result);
            echo 'Sent email unsuccessful';
        }
    }

    /**
     * @desc build email array from string
     * @param $str
     * @return array
     */
    public function buildCcBcc($str)
    {
        if (empty($str))
            return null;

        $recs = explode(',', $str);
        $rets = array();
        foreach ($recs as $email) {
            $rets[] = array(trim($email), trim($email));
        }
        return $rets;
    }

    /**
     * @static store email properties to database for cron job
     * @param $type
     * @param $senderName
     * @param $senderEmail
     * @param $recipientName
     * @param $recipientEmail
     * @param $subject
     * @param $content
     * @param $attachments
     * @param $cc
     * @param $bcc
     * @return bool
     */
    public static function createEmail($type, $senderName, $senderEmail, $recipientName, $recipientEmail, $subject, $content, $attachments = null, $cc = null, $bcc = null)
    {
        $email = new self;
        $email->type = $type;
        $email->sender_name = $senderName;
        $email->sender_email = $senderEmail;
        $email->recipient_name = $recipientName;
        $email->recipient_email = $recipientEmail;
        $email->subject = $subject;
        $email->cc = $cc;
        $email->bcc = $bcc;
        $email->content = $content;

        //print_r($email->attributes);die;
        if ($email->save())
            return true;

        return false;
    }

}