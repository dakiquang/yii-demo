<?php
/**
* This is the model class for table "newsletter_emails".
*
* The followings are the available columns in table 'newsletter_emails':
* @property integer $id
* @property integer $newsletter_id
* @property integer $type
* @property string $sender_email
* @property string $sender_name
* @property string $recipient_email
* @property string $recipient_name
* @property string $cc
* @property string $bcc
* @property string $subject
* @property string $content
* @property integer $is_sent
* @property integer $sent_time
* @property integer $created_time
*/
class NewsletterEmail extends CActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return NewsletterEmail the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'newsletter_emails';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('sender_email, sender_name, subject, content', 'required'),
			array('newsletter_id, type, is_sent, sent_time, created_time', 'numerical', 'integerOnly' => true),
			array('sender_email, sender_name, recipient_email, recipient_name, cc, bcc', 'length', 'max' => 255),
			array('id, newsletter_id, type, sender_email, sender_name, recipient_email, recipient_name, cc, bcc, subject, content, is_sent, sent_time, created_time', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'newsletter_id' => 'Newsletter',
			'type' => 'Type',
			'sender_email' => 'Sender Email',
			'sender_name' => 'Sender Name',
			'recipient_email' => 'Recipient Email',
			'recipient_name' => 'Recipient Name',
			'cc' => 'Cc',
			'bcc' => 'Bcc',
			'subject' => 'Subject',
			'content' => 'Content',
			'is_sent' => 'Is Sent',
			'sent_time' => 'Sent Time',
			'created_time' => 'Created Time',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('newsletter_id', $this->newsletter_id);
		$criteria->compare('type', $this->type);
		$criteria->compare('sender_email', $this->sender_email, true);
		$criteria->compare('sender_name', $this->sender_name, true);
		$criteria->compare('recipient_email', $this->recipient_email, true);
		$criteria->compare('recipient_name', $this->recipient_name, true);
		$criteria->compare('cc', $this->cc, true);
		$criteria->compare('bcc', $this->bcc, true);
		$criteria->compare('subject', $this->subject, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('is_sent', $this->is_sent);
		$criteria->compare('sent_time', $this->sent_time);
		$criteria->compare('created_time', $this->created_time);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}