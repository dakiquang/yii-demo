<?php
/**
* This is the model class for table "products".
*
* The followings are the available columns in table 'products':
* @property integer $id
* @property integer $catagory_id
* @property integer $created_by
* @property string $name
* @property string $description
* @property string $photo
* @property integer $created_time
*/
class Product extends CActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return Product the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'products';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('catagory_id, created_by, name, created_time', 'required'),
			array('catagory_id, created_by, created_time', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 64),
			array('description, photo', 'safe'),
			array('id, catagory_id, created_by, name, description, photo, created_time', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'catagory_id' => 'Catagory',
			'created_by' => 'Created By',
			'name' => 'Name',
			'description' => 'Description',
			'photo' => 'Photo',
			'created_time' => 'Created Time',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('catagory_id', $this->catagory_id);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('photo', $this->photo, true);
		$criteria->compare('created_time', $this->created_time);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}