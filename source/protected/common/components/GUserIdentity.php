<?php

class GUserIdentity extends CUserIdentity 
{
    private $_id;
    const ERROR_ACOUNT_DEACTIVE = 3;
    
    public function authenticate()
    {
        $user = User::model()->findByAttributes(array('email' => $this->username));
        if ($user === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if ($user->password !== Hp::md5($this->password))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $user->id;
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
    
    public function getId()
    {
        return $this->_id;
    }
}