<?php

//include email lib
Yii::import("common.extensions.phpmailer.*");
require_once('class.phpmailer.php');

class EmailHelper
{

    /**
     * @desc phpMailer wrapper to send email based on configured send mail method in application configuration
     * @param $subject
     * @param $htmlContent
     * @param $plainContent
     * @param $addresses
     * @param $from
     * @param $attachments
     * @param $replyTo
     * @return int 
     */
    public static function _send($subject, $htmlContent, $plainContent, $addresses, $from, $attachments, $replyTo)
    {
        $mail = new PHPMailer(true);  // the true param means it will throw exceptions on errors, which we need to catch      
        $mail->IsSMTP();              // telling the class to use SMTP 
        $mail->CharSet = Hp::param("mailSender", "charset");

        try {
            //$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
            $mail->SMTPAuth = true;
            //$mail->SMTPSecure = SMTP_SECURE;              // sets the prefix to the servier
            $mail->Host = Hp::param("mailSender", "host");                // sets GMAIL as the SMTP server
            //$mail->Port       = Hp::param("mailSender", "port") ;                // set the SMTP port for the GMAIL server
            $mail->Username = Hp::param("mailSender", "username");
            $mail->Password = Hp::param("mailSender", "password");

            // set from mail
            if (is_array($from) && count($from) > 0) {
                $mail->SetFrom($from[0], $from[1]);
                $mail->AddCustomHeader("From: " . $from[0] . " <" . $from[1] . ">");
            } else {
                $mail->SetFrom(Hp::param("mailSender", "from"), Hp::param("mailSender", "name"));
                $mail->AddCustomHeader("From: " . Hp::param("mailSender", "from") . " <" . Hp::param("mailSender", "name") . ">");
            }

            //add reply to
            if (is_array($replyTo) && count($replyTo) > 1) {
                $mail->AddReplyTo($replyTo[0], $replyTo[1]);
            } else {
                $mail->AddReplyTo(Hp::param("mailSender", "reply"), Hp::param("mailSender", "name"));
            }

            $mail->Subject = $subject;
            $mail->AltBody = $plainContent;
            $mail->MsgHTML($htmlContent);
            //add receivers emails:            
            foreach ($addresses as $email) {
                $mail->AddAddress($email[0], $email[1]);
            }
            //add attachment files
            if (is_array($attachments)) {
                foreach ($attachments as $file) {
                    $mail->AddAttachment($file);
                }
            }
            //print_r($mail);die;
            $mail->Send();
            return 1;
        } catch (phpmailerException $e) {
            Yii::log($e->errorMessage());
            //echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            Yii::log($e->errorMessage());
            //echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    /**
     * @desc Send mail with activation code to registed member
     * 
     * @param $params
     * @param $subject
     * @param $addresses
     * @param $template
     * @param $from
     * @param $attachments
     * @param $replyTo
     * @return type 
     */
    public static function sendToMember($params, $subject, $addresses, $template, $from = null, $attachments = null, $replyTo = null)
    {
        $body = self::getBodyTemplate($params, $template);
        return self::_send($subject, $body, strip_tags($body), $addresses, $from, $attachments, $replyTo);
    }

    /**
     * @static Send email user application configured mail method and server information
     * @param $params
     * @param $subject
     * @param $addresses
     * @param null $template
     * @param null $attachments
     * @param null $from
     * @param null $replyTo
     * @return int
     */
    public static function send($params, $subject, $addresses, $template = null, $attachments = null, $from = null, $replyTo = null)
    {
        //$body = self::getBodyTemplate($params, $template);
        return self::_send($subject, $params['MailContent'], strip_tags($params['MailContent']), $addresses, $attachments, $from, $replyTo);
    }

    /**
     * @desc get Body of email by parse template
     * 
     * @param mixed $params
     * @param mixed $template
     */
    public static function getBodyTemplate($params, $template)
    {
        if (!is_null($template)) {
            $template = self::loadMailTemplate($template);
            if (!empty($template))
                $body = self::parseMailContent($params, $template);
        }else {
            $body = $params['MailContent'];
        }
        return $body;
    }

    /**
     * @desc send mail by console
     * @param type $subject
     * @param type $content
     * @param type $from
     * @param type $sendto
     * @param type $attachments
     * @param type $cc
     * @param type $bcc
     * @return boolean 
     */
    public static function queueSend($subject, $content, $from, $sendto, $attachments = null, $cc = null, $bcc = null)
    {
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        $mail->CharSet = Hp::param("mailSender", "charset");

        //$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true;                     // enable SMTP authentication
        $mail->SMTPSecure = Hp::param("mailSender", "secure");              // sets the prefix to the servier
        $mail->Host = Hp::param("mailSender", "host");                // sets GMAIL as the SMTP server
        $mail->Port = Hp::param("mailSender", "port");                // set the SMTP port for the GMAIL server
        $mail->Username = Hp::param("mailSender", "username");
        $mail->Password = Hp::param("mailSender", "password");

        try {
            $mail->SetFrom($from[0], $from[1]);
            $mail->AddCustomHeader("From: " . $from[0] . " <" . $from[1] . ">");
            $mail->AddReplyTo($from[0], $from[1]);

            //set mail receive
            $mail->AddAddress($sendto[0], $sendto[1]);

            //set mail cc
            if (is_array($cc)) {
                foreach ($cc as $element) {
                    $mail->AddCC($element[0], $element[1]);
                }
            }

            //set mail bcc
            if (is_array($bcc)) {
                foreach ($bcc as $element) {
                    $mail->AddBCC($element[0], $element[1]);
                }
            }

            //add attachment files
            if (is_array($attachments)) {
                foreach ($attachments as $file) {
                    clearstatcache();
                    $mail->AddAttachment($file);
                }
            }

            //set subject and body mail
            $mail->Subject = $subject;
            $mail->IsHTML(true);

            //$mail->Body = $content;
            //$mail->Body = addslashes($content);
            $mail->Body = $content;

            //print_r($mail);die;
            $mail->Send();
            return 1;
        } catch (phpmailerException $e) {
            Yii::log($e->errorMessage());
        } catch (Exception $e) {
            Yii::log($e->errorMessage());
        }
    }

    /**
     * @desc send plain mail
     * @param $subject
     * @param $body
     * @param $addresses
     * @param $from
     * @param $attachments
     * @param $replyTo
     * @return mixed
     */
    public static function sendPlainMail($subject, $body, $addresses, $from = null, $attachments = null, $replyTo = null)
    {
        return self::_send($subject, $body, strip_tags($body), $addresses, $from, $attachments, $replyTo);
    }

    /**
     * @desc load mail template from file
     * @param type $filename
     * @return string 
     */
    private static function loadMailTemplate($filename)
    {
        if ($fp = fopen($filename, 'rb')) {
            $template = fread($fp, filesize($filename));
            fclose($fp);
            return $template;
        } else {
            return '';
        }
    }

    /**
     * @desc parse mail template and assign data
     * @param array $parameter
     * @param string $template
     * @return string
     */
    public static function parseMailContent($parameter, $template)
    {
        $matches = array('');
        while (count($matches) > 0) {
            if (preg_match("/\[([a-zA-Z0-9_])*\]/", $template, $matches)) {
                $tag = $matches[0];
                $property = substr($tag, 1, strlen($tag) - 2);
                if ($property != '') {
                    $value = $parameter[$property];
                    $template = str_replace($tag, $value, $template);
                }
            }
        } // while

        return $template;
    }

    /**
     * @desc get data from mail template with parse parameters from DB email template
     * @param $template
     * @param $params
     * @param $field : body, subject, sender_name, sender_email, receiver_name, receiver_email
     * @return mixed
     */
    public static function getDataFromTemplate($template, $params, $field = "body")
    {
        $ret = "";
        switch ($field) {
            case "body":
                $ret = mb_convert_encoding($template->body, 'UTF-8', mb_detect_encoding($template->body, 'UTF-8, ISO-8859-1', true));
                break;
            case "subject":
                $ret = mb_convert_encoding($template->subject, 'UTF-8', mb_detect_encoding($template->subject, 'UTF-8, ISO-8859-1', true));
                break;
            case "sender_name":
                $ret = mb_convert_encoding($template->sender_name, 'UTF-8', mb_detect_encoding($template->sender_name, 'UTF-8, ISO-8859-1', true));
                break;
            case "sender_email":
                $ret = mb_convert_encoding($template->sender_email, 'UTF-8', mb_detect_encoding($template->sender_email, 'UTF-8, ISO-8859-1', true));
                break;
            case "receiver_name":
                $ret = mb_convert_encoding($template->receiver_name, 'UTF-8', mb_detect_encoding($template->receiver_name, 'UTF-8, ISO-8859-1', true));
                break;
            case "receiver_email":
                $ret = mb_convert_encoding($template->receiver_email, 'UTF-8', mb_detect_encoding($template->receiver_email, 'UTF-8, ISO-8859-1', true));
                break;
            default:
                break;
        }

        return self::parseMailContent($params, $ret);
    }

}