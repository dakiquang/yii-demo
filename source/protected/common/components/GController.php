<?php

class GController extends CController 
{
	public $layout = 'main';
    
    /**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() 
	{
		if ($error = Yii::app()->errorHandler->error) {
			print_r($error);
		}
	}
}