-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2013 at 11:35 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ourauctions`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` tinytext,
  `sender_name` varchar(255) NOT NULL,
  `sender_email` varchar(255) NOT NULL,
  `receiver_name` varchar(255) NOT NULL,
  `receiver_email` varchar(255) NOT NULL,
  `subject` tinytext NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=4 ;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `name`, `description`, `sender_name`, `sender_email`, `receiver_name`, `receiver_email`, `subject`, `body`) VALUES
(1, 'CREATE_ACCOUNT', 'This template is to send an email for create account', '[SENDER_NAME]', '[SENDER_EMAIL]', '[RECEIVER_NAME]', '[RECEIVER_EMAIL]', 'Welcome to Our Auctions website!', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\r\n\r\n	<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellspacing="5" cellpadding="0" cellspacing="0">\r\n		<tbody>\r\n			<tr style="background:#7b4596">\r\n				  <td>\r\n				  <img src="http://ourauction.com.au/images/spacer.gif" alt="" width="1" height="12" /></a>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				  <td style="padding:14px;">\r\n				  <h1 style="width:300px;float:left;color:#7b4596;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Wellcome to OurAuction website</h1>\r\n				  <a href="http://ourauction.com.au"><img src="http://ourauction.com.au/images/logo.png" id="logo" alt="logo" style="width: 120px;float:right;margin-left:60px;" ></a>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td style="padding:10px;">\r\n\r\n					<div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\r\n\r\n						<p style="padding:14px;color:#222222;line-height:18px;">\r\n				Hi [NAME],<br /><br />\r\n	\r\n				You will need to click the link below to activate your OurAuction account.<br><a href=''[LINK_ACTIVE]''>Link active account</a><br>You may need to manually copy &amp; paste the URL into the address bar of your browser if the text above is not linked. Please watch for the URL wrapping onto 2 lines.			  \r\n				\r\n				<br>\r\n				<br>\r\n				Sincerely,<br>\r\n				The Our Auctions.\r\n			  </p>  \r\n			</div>\r\n				   \r\n				</td>\r\n			</tr>\r\n			\r\n		</tbody>\r\n	</table>\r\n\r\n	<table style="font-family:Arial, Helvetica, sans-serif;font-size:11px;color:#8b8b8b;border:none;border-collapse:collapse;background-color:#ccc;margin-top:20px;" cellspacing="5" cellpadding="0" cellspacing="0" width="100%">\r\n		<tbody>\r\n			<tr>\r\n				  <td>\r\n				  <div style="margin-right:auto;margin-left:auto;width:600px;">\r\n					  <p style="width:440px;float:left;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:14px;font-weight:normal;"><br/>\r\n						If you are have any questions, please feel free to contact us at <a style="color:#fff" href="mailto:contact@ourauction.com.au">contact@ourauction.com.au</a> This email is a post only email. \r\n					  </p>\r\n					  <a href="http://ourauction.com.au"><img src="http://ourauction.com.au/images/email/logo-bw.png" alt="logo" style="width: 100px;float:right;margin-left:60px;" ></a>\r\n				</div>\r\n				</td>\r\n			</tr>\r\n		</tbody>\r\n	</table>\r\n\r\n</div>'),
(2, 'FORGOT_PASSWORD', 'This template is to send an email for got password.', '[SENDER_NAME]', '[SENDER_EMAIL]', '[RECEIVER_NAME]', '[RECEIVER_EMAIL]', 'You request a new OurAuction password.', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\r\n\r\n	<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellspacing="5" cellpadding="0" cellspacing="0">\r\n		<tbody>\r\n			<tr style="background:#7b4596">\r\n				  <td>\r\n				  <img src="http://ourauction.com.au/images/spacer.gif" alt="" width="1" height="12" /></a>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				  <td style="padding:14px;">\r\n				  <h1 style="width:300px;float:left;color:#7b4596;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Wellcome to OurAuction website</h1>\r\n				  <a href="http://ourauction.com.au"><img src="http://ourauction.com.au/images/logo.png" id="logo" alt="logo" style="width: 120px;float:right;margin-left:60px;" ></a>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td style="padding:10px;">\r\n\r\n					<div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\r\n\r\n						<p style="padding:14px;color:#222222;line-height:18px;">\r\n				Hi [NAME],<br /><br />\r\n	\r\n				We have received a password change request for your Our Auctions account: [EMAIL]<br>If you made this request, then please click on the link below <a href=''[LINK]''>Link for got password account</a><br>If you did not ask to change your password, then please ignore this email.			  \r\n				\r\n				<br>\r\n				<br>\r\n				Sincerely,<br>\r\n				The Our Auctions.\r\n			  </p>  \r\n			</div>\r\n				   \r\n				</td>\r\n			</tr>\r\n			\r\n		</tbody>\r\n	</table>\r\n\r\n	<table style="font-family:Arial, Helvetica, sans-serif;font-size:11px;color:#8b8b8b;border:none;border-collapse:collapse;background-color:#ccc;margin-top:20px;" cellspacing="5" cellpadding="0" cellspacing="0" width="100%">\r\n		<tbody>\r\n			<tr>\r\n				  <td>\r\n				  <div style="margin-right:auto;margin-left:auto;width:600px;">\r\n					  <p style="width:440px;float:left;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:14px;font-weight:normal;"><br/>\r\n						If you are have any questions, please feel free to contact us at <a style="color:#fff" href="mailto:contact@ourauction.com.au">contact@ourauction.com.au</a> This email is a post only email. \r\n					  </p>\r\n					  <a href="http://ourauction.com.au"><img src="http://ourauction.com.au/images/email/logo-bw.png" alt="logo" style="width: 100px;float:right;margin-left:60px;" ></a>\r\n				</div>\r\n				</td>\r\n			</tr>\r\n		</tbody>\r\n	</table>\r\n\r\n</div>'),
(3, 'CHANGE_PASSWORD', 'This template is to send an email change password successful.', '[SENDER_NAME]', '[SENDER_EMAIL]', '[RECEIVER_NAME]', '[RECEIVER_EMAIL]', 'You change password on OurAuction successful.', '<div style="width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;">\r\n\r\n	<table style="font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;" cellspacing="5" cellpadding="0" cellspacing="0">\r\n		<tbody>\r\n			<tr style="background:#7b4596">\r\n				  <td>\r\n				  <img src="http://ourauction.com.au/images/spacer.gif" alt="" width="1" height="12" /></a>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				  <td style="padding:14px;">\r\n				  <h1 style="width:300px;float:left;color:#7b4596;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;">Wellcome to OurAuction website</h1>\r\n				  <a href="http://ourauction.com.au"><img src="http://ourauction.com.au/images/logo.png" id="logo" alt="logo" style="width: 120px;float:right;margin-left:60px;" ></a>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td style="padding:10px;">\r\n\r\n					<div style="background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;">\r\n\r\n						<p style="padding:14px;color:#222222;line-height:18px;">\r\n				Hi [NAME],<br /><br />\r\n	\r\n				You password change on OurAuction successful.			  \r\n				\r\n				<br>\r\n				<br>\r\n				Sincerely,<br>\r\n				The Our Auctions.\r\n			  </p>  \r\n			</div>\r\n				   \r\n				</td>\r\n			</tr>\r\n			\r\n		</tbody>\r\n	</table>\r\n\r\n	<table style="font-family:Arial, Helvetica, sans-serif;font-size:11px;color:#8b8b8b;border:none;border-collapse:collapse;background-color:#ccc;margin-top:20px;" cellspacing="5" cellpadding="0" cellspacing="0" width="100%">\r\n		<tbody>\r\n			<tr>\r\n				  <td>\r\n				  <div style="margin-right:auto;margin-left:auto;width:600px;">\r\n					  <p style="width:440px;float:left;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:14px;font-weight:normal;"><br/>\r\n						If you are have any questions, please feel free to contact us at <a style="color:#fff" href="mailto:contact@ourauction.com.au">contact@ourauction.com.au</a> This email is a post only email. \r\n					  </p>\r\n					  <a href="http://ourauction.com.au"><img src="http://ourauction.com.au/images/email/logo-bw.png" alt="logo" style="width: 100px;float:right;margin-left:60px;" ></a>\r\n				</div>\r\n				</td>\r\n			</tr>\r\n		</tbody>\r\n	</table>\r\n\r\n</div>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
