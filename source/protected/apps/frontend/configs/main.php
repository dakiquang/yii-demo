<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'OurAuctions',
    'preload' => array('bootstrap'),
    'components' => array(
        'request' => array(
            'class' => 'HttpRequest',
        ),
        'bootstrap' => array(
            'responsiveCss' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => require ("rules.php"),
        ),
    ),
    'modules' => array(
        'onlinehelp',
    ),
);