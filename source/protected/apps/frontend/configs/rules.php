<?php
/**
 * User: Ceres LLC
 * Date: 3/19/13
 * Time: 2:54 PM
 */

return array(
    '<view:about>' => 'site/page',
    '<view:pricing>' => 'site/page',
    '<view:features>' => 'site/page',
    '<view:blog>' => 'site/page',
    '<view:portfolio>' => 'site/page',
    '<view:coming_soon>' => 'site/page',
    '<view:faq>' => 'site/page',
    'http://<domain:\w+>.ourauctions.net/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    'https://<domain:\w+>.ourauctions.net/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    
);