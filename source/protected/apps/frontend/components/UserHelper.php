<?php
Class UserHelper 
{
    public static function sendEmailNewAcountUser($userID)
    {
        // Email send creative non connect
        $user = User::model()->findByPk($userID);
        $template = EmailTemplate::getTemplate('CREATE_ACCOUNT');
        $subject  = EmailHelper::getDataFromTemplate($template, array(), 'subject');

        $params  = array(
            "NAME" => $user->first_name,
            "LINK_ACTIVE" => url('user/verify', array('vr' => $user->activation_key)),
        );
        $message = EmailHelper::getDataFromTemplate($template, $params);

        // save mail to cronjob
        Email::createEmail(
            Email::URGENT, 
            param('mailSender', 'name'), 
            param('mailSender', 'from'), 
            $user->first_name, 
            $user->email, // creative email
            $subject, $message
        );
    }
    
    public static function sendEmailSendForgotPassword($user)
    {
        // Email send creative non connect
        $template = EmailTemplate::getTemplate('FORGOT_PASSWORD');
        $subject  = EmailHelper::getDataFromTemplate($template, array(), 'subject');

        $params  = array(
            "NAME" => $user->first_name,
            "EMAIL" => $user->email,
            "LINK" => url('site/forgotPassword', array('fg' => $user->passwordResetString)),
        );
        $message = EmailHelper::getDataFromTemplate($template, $params);

        // save mail to cronjob
        $result = Email::createEmail(
            Email::URGENT, 
            param('mailSender', 'name'), 
            param('mailSender', 'from'), 
            $user->first_name, 
            $user->email, // creative email
            $subject, $message
        );
        
        return $result;
    }
    
    public static function sendEmailChangePassword($user)
    {
        // Email send creative non connect
        $template = EmailTemplate::getTemplate('CHANGE_PASSWORD');
        $subject  = EmailHelper::getDataFromTemplate($template, array(), 'subject');

        $params  = array(
            "NAME" => $user->first_name,
        );
        $message = EmailHelper::getDataFromTemplate($template, $params);

        // save mail to cronjob
        Email::createEmail(
            Email::URGENT, 
            param('mailSender', 'name'), 
            param('mailSender', 'from'), 
            $user->first_name, 
            $user->email, // creative email
            $subject, $message
        );
    }
    
}