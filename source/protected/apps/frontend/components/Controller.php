<?php

class Controller extends GController
{
    public $breadCrumbs = array();
    public $leftMenu = array();
    public $pageTitle;

    protected function performAjaxValidation($models, $formId = '')
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] == $formId) {
            echo  CActiveForm::validate($models);
            app()->end();
        }
    }

    /**
     * @param string $modelClass
     * @param  $id
     * @return ActiveRecord
     * @throws CHttpException
     */
    protected function loadModel($modelClass, $id = null)
    {
        if ($id == null)
            return new $modelClass();
        $modelStatic = call_user_func(array($modelClass, 'model'));
        $instance = $modelStatic->findByPk($id);
        if ($instance == null)
            throw new CHttpException(404, 'Page not found.');
        return $instance;
    }

    protected function beforeAction($action)
    {
        $ref = parent::beforeAction($action);
        
        return $ref;
    }
}