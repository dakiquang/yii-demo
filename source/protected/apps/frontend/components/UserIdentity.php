<?php

class UserIdentity extends GUserIdentity
{
    private $_id;
    
    public function authenticate()
    {
        $user = User::model()->findByAttributes(array('email' => $this->username));
        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        else if ($user->password !== Utils::md5($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        else if ($user->status == User::STATUS_DEACTIVE) {
            $this->errorCode = self::ERROR_ACOUNT_DEACTIVE;
        }
        else {
            $this->_id = $user->id;
            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode;
    }
    
    public function getId()
    {
        return $this->_id;
    }
}