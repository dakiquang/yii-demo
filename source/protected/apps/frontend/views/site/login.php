<?php
$this->pageTitle = 'Login Page';
?>  
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/signin.css">
<div id="box_login">
    <div class="container">
        <div class="span12 box_wrapper">
            <div class="span12 box">
                <div>
                    <div class="head">
                        <h4>Log in to your account</h4>
                    </div>
                    <div class="form">
                        <?php if (Yii::app()->user->hasFlash('success')) { ?>
                            <div class="success">
                                <?php echo Yii::app()->user->getFlash('success'); ?>
                            </div>
                        <?php }
                        if (!empty($message)) {
                            ?>      
                            <div id="status_message">
                                <span class="flash-message flash-boner"><?php echo $message ?></span>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="social">
                        <a class="face_login" href="#">
                            <span class="face_icon">
                                <img src="<?php echo Yii::app()->baseUrl ?>/img/i_face.png" alt="fb" alt=""/>
                            </span>
                            <span class="text">Sign in with Facebook</span>
                        </a>
                        <div class="division">
                            <hr class="left">
                            <span>or</span>
                            <hr class="right">
                        </div>
                    </div>
                    <div class="form">
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id'                     => 'login-form',
                            'enableClientValidation' => true,
                            'clientOptions'          => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        echo $form->textField($model, 'username', array('placeholder' => 'Email'));
                        echo $form->passwordField($model, 'password', array('placeholder' => 'Password'));
                        ?>
                        <div class="remember">
                            <div class="left">
                                <?php echo $form->checkBox($model, 'rememberMe'); ?>
                                <label for="remember_me">Remember me</label>
                            </div>
                            <div class="right">
                                <a data-target="#myModal" data-toggle="modal">Forgot password?</a>
                            </div>
                        </div>
                        <input type="submit" class="btn" value="Sign in"/>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
            <p class="already">Don't have an account? <a href="signup.html"> Sign up</a></p>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function () {
            $('#forget_success').hide();
    });
    function sendMailSubmit() {
        dt = {email: jQuery('#forgot_sign_in_email').val()};
        
        if (!validEmail(dt['email'])) {
            $(".error").html('<p>Please enter an valid email!</p>');  
            $("#forgot_sign_in_email").focus();  
            return false;  
        } 
        
        $(".modal-footer").html("<img src='images/loading.gif'/>");
        $.post('<?php echo url("site/forgotPassword") ?>', dt, function(data) {
            var d = eval('(' + data + ')');
            if (!d.success) {
                $(".error").html(d.message);
                $(".modal-footer").html('<a id="yw0" class="btn btn-primary" href="#" onclick="return sendMailSubmit();" >Submit</a>');
            } else {
                $(".error").hide();
                $("#forget_form").hide();
                $("#forget_success").show();
            }
        });
        return false;
    }
    
    function validEmail(v) {
        var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
        return (v.match(r) == null) ? false : true;
    }

</script>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'myModal')); ?>
    
<div class="modal-header">
    <a class="close" data-dismiss="modal">x</a>
    <h4>Forgot password ?</h4>
</div>

<div class="error" style="color: #B94A48; padding: 10px 0 0 15px;">
</div>
    
<div class="modal-body" id ="forget_form">
    <p>Enter your email address below, and we'll send you a link to your account.</p>
    <label>Email address  </lable>
    <input type="text" id="forgot_sign_in_email" autocomplete="off" >
</div>

<div class="modal-body" id="forget_success">
    <h3>Thanks!</h3>
    <p>An email explaining how to reset your password has been sent to you.</p>
</div>
    
<div class="modal-footer">
    <a id="yw0" class="btn btn-primary" href="#" onclick="return sendMailSubmit();" >Submit</a>
</div>
<?php $this->endWidget(); ?>