<?php cs()->registerCssFile('/css/signup.css'); ?>
<div id="box_sign">
    <div class="container">
        <div class="span12 box_wrapper">
            <div class="span12 box">
                <div>
                    <div class="head">
                        <h4>Forgot password</h4>
                    </div>
                    <div class="form">

                        <?php if (Yii::app()->user->hasFlash('success')) { ?>
                            <div class="success">
                                <?php echo Yii::app()->user->getFlash('success'); ?>
                            </div>
                        <?php } else { ?>

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id'                   => 'changePassword-form',
                            'enableAjaxValidation' => false,
                        ));
                        ?>

                        <div class="row <?php echo ($form->error($model, 'password') ? 'error' : '') ?>">
                            <div class="field-left">
                                <?php echo $form->labelEx($model, 'password'); ?>
                            </div>
                            <div class="text_error">
                                <div class="field-right">
                                    <?php echo $form->passwordField($model, 'password', array('size'        => 32, 'maxlength'   => 32, 'placeholder' => 'New Password')); ?>
                                </div>
                                <div class="field-error">
                                    <?php echo $form->error($model, 'password'); ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="row <?php echo ($form->error($model, 'rePassword') ? 'error' : '') ?>">
                            <div class="field-left">
                                <?php echo $form->labelEx($model, 'rePassword'); ?>
                            </div>
                            <div class="text_error">
                                <div class="field-right">
                                    <?php echo $form->passwordField($model, 'rePassword', array('size'        => 32, 'maxlength'   => 32, 'placeholder' => 'Confirm Password')); ?>
                                </div>
                                <div class="field-error">
                                    <?php echo $form->error($model, 'rePassword'); ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="row button">
                            <?php echo CHtml::submitButton('Save', array('class' => 'btn')); ?>
                        </div>

                        <?php $this->endWidget(); ?>
                        
                        <?php } ?>

                    </div><!-- form -->
                </div>
            </div>
        </div>
    </div>
</div>

