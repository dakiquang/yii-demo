<?php

$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true,
    'fade' => true,
    'closeText' => '×',
    'alerts' => array(
        'success' => array('block' => true, 'fade' => true, 'closeText' => '×'), // success, info, warning, error or danger
        'error' => array('block' => true, 'fade' => true, 'closeText' => '×'),
        'info' => array('block' => true, 'fade' => true, 'closeText' => '×'),
        'warning' => array('block' => true, 'fade' => true, 'closeText' => '×'),
        'danger' => array('block' => true, 'fade' => true, 'closeText' => '×'),
    ),
));
