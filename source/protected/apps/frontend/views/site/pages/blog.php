<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/css/blog.css">
<div id="blog_wrapper">
    <div class="container">
        <div class="row">

            <div class="span8">

                <h1 class="header">
                    Blog
                    <hr>
                </h1>

                <div class="post">
                    <div class="row">
                        <div class="span3">
                            <a href="blog-post.html">
                                <img class="main_pic" alt="main pic" src="<?php echo Yii::app()->baseUrl;?>/img/pic_blog.png" />
                            </a>
                        </div>
                        <div class="span4 info">
                            <a href="blog-post.html">
                                <h3>Measure theme blog post</h3>
                            </a>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                            <div class="post_info">
                                <div class="author">
                                    Alejandra Galvan
                                </div>
                                <div class="date">
                                    Oct 12, 2012
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="blog-post.html" class="btn">more</a>
                </div>

                <div class="post">
                    <div class="row">
                        <div class="span3">
                            <a href="blog-post.html">
                                <img class="main_pic" alt="main pic" src="<?php echo Yii::app()->baseUrl;?>/img/pic_blog.png" />
                            </a>
                        </div>
                        <div class="span4 info">
                            <a href="blog-post.html">
                                <h3>Measure theme blog post</h3>
                            </a>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                            <div class="post_info">
                                <div class="author">
                                    Alejandra Galvan
                                </div>
                                <div class="date">
                                    Oct 12, 2012
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="blog-post.html" class="btn">more</a>
                </div>

                <div class="post">
                    <div class="row">
                        <div class="span3">
                            <a href="blog-post.html">
                                <img class="main_pic" alt="main pic" src="<?php echo Yii::app()->baseUrl;?>/img/pic_blog.png" />
                            </a>
                        </div>
                        <div class="span4 info">
                            <a href="blog-post.html">
                                <h3>Measure theme blog post</h3>
                            </a>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                            <div class="post_info">
                                <div class="author">
                                    Alejandra Galvan
                                </div>
                                <div class="date">
                                    Oct 12, 2012
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="blog-post.html" class="btn">more</a>
                </div>

                <div class="post">
                    <div class="row">
                        <div class="span3">
                            <a href="blog-post.html">
                                <img class="main_pic" alt="main pic" src="<?php echo Yii::app()->baseUrl;?>/img/pic_blog.png" />
                            </a>
                        </div>
                        <div class="span4 info">
                            <a href="blog-post.html">
                                <h3>Measure theme blog post</h3>
                            </a>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                            <div class="post_info">
                                <div class="author">
                                    Alejandra Galvan
                                </div>
                                <div class="date">
                                    Oct 12, 2012
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="blog-post.html" class="btn">more</a>
                </div>

                <div class="pagination">
                    <ul>
                        <li><a href="#">Prev</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
                </div>

            </div>

            <div class="span3 sidebar offset1">

                <input type="text" class="input-large search-query" placeholder="Search">

                <h4 class="sidebar_header">
                    Menu
                </h4>

                <ul class="sidebar_menu">
                    <li><a href="#">Suspendisse Semper Ipsum</a></li>
                    <li><a href="#">Maecenas Euismod Elit</a></li>
                    <li><a href="#">Suspendisse Semper Ipsum</a></li>
                    <li><a href="#">Maecenas Euismod Elit</a></li>
                    <li><a href="#">Suspendisse Semper Ipsum</a></li>
                </ul>

                <h4 class="sidebar_header">
                    Recent posts
                </h4>

                <ul class="recent_posts">
                    <li>
                        <div class="row">
                            <div class="span1">
                                <a href="blog-post.html">
                                    <img class="thumb" alt="thumb post" src="<?php echo Yii::app()->baseUrl;?>/img/pic_blog.png" />
                                </a>
                            </div>
                            <div class="span2">
                                <a class="link" href="blog-post.html">Suspendisse Semper Ipsum</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="span1">
                                <a href="blog-post.html">
                                    <img class="thumb" alt="thumb post" src="<?php echo Yii::app()->baseUrl;?>/img/pic_blog.png" />
                                </a>
                            </div>
                            <div class="span2">
                                <a class="link" href="blog-post.html">Maecenas Euismod Elit</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="span1">
                                <a href="blog-post.html">
                                    <img class="thumb" alt="thumb post" src="<?php echo Yii::app()->baseUrl;?>/img/pic_blog.png" />
                                </a>
                            </div>
                            <div class="span2">
                                <a class="link" href="blog-post.html">Suspendisse Semper Ipsum</a>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>

        </div>
    </div>
</div>