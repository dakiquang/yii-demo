<?php cs()->registerCssFile('/css/signup.css'); ?>

<div>
    <div class="head">
        <h3>Change password</h3>
    </div>
    <div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id'                   => 'changePassword-form',
            'enableAjaxValidation' => false,
        ));
        ?>

        <div class="row <?php echo ($form->error($model, 'oldPassword') ? 'error' : '') ?>">
            <div class="field-left">
                <?php echo $form->labelEx($model, 'oldPassword'); ?>
            </div>
            <div class="text_error">
                <div class="field-right">
                    <?php echo $form->passwordField($model, 'oldPassword', array('size'        => 50, 'maxlength'   => 128, 'placeholder' => 'Old Password')); ?>
                </div>
                <div class="field-error">
                    <?php echo $form->error($model, 'oldPassword'); ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>

        <div class="row <?php echo ($form->error($model, 'newPassword') ? 'error' : '') ?>">
            <div class="field-left">
                <?php echo $form->labelEx($model, 'newPassword'); ?>
            </div>
            <div class="text_error">
                <div class="field-right">
                    <?php echo $form->passwordField($model, 'newPassword', array('size'        => 32, 'maxlength'   => 32, 'placeholder' => 'New Password')); ?>
                </div>
                <div class="field-error">
                    <?php echo $form->error($model, 'newPassword'); ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>

        <div class="row <?php echo ($form->error($model, 'repeatNewPassword') ? 'error' : '') ?>">
            <div class="field-left">
                <?php echo $form->labelEx($model, 'repeatNewPassword'); ?>
            </div>
            <div class="text_error">
                <div class="field-right">
                    <?php echo $form->passwordField($model, 'repeatNewPassword', array('size'        => 32, 'maxlength'   => 32, 'placeholder' => 'Confirm Password')); ?>
                </div>
                <div class="field-error">
                    <?php echo $form->error($model, 'repeatNewPassword'); ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>

        <div class="row button">
            <?php echo CHtml::submitButton('Update', array('class' => 'btn')); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>
