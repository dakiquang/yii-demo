<?php cs()->registerCssFile('/css/signup.css'); ?>
<div id="box_sign">
    <div class="container">
        <div class="span12 box_wrapper">
            <div class="span12 box">
                <div>
                    <div class="head">
                        <h3>Create your account</h3>
                    </div>
                    <div class="form" style="text-align: left;">
                    <?php
                    $this->widget('bootstrap.widgets.TbAlert', array(
                        'block'     => true, // display a larger alert block?
                        'fade'      => true, // use transitions?
                        'closeText' => 'x', // close link text - if set to false, no close link is displayed
                        'alerts'    => array(// configurations per alert type
                            'success' => array(
                                'block' => true,
                                'fade'  => true,
                            ), // success, info, warning, error or danger
                        ),
                    ));
                    ?>
                    </div>
                    <?php 
                        echo $this->renderPartial('_formRegister', array('model' => $model)); 
                    ?>
                </div>
            </div>
            <p class="already">Already have an account? 
                <a href="<?php echo url('site/login')?>">Sign in</a></p>
        </div>
    </div>
</div>