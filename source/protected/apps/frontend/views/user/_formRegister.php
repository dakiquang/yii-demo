<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'                   => 'user-form',
        'enableAjaxValidation' => false,
        'htmlOptions'          => array('enctype' => 'multipart/form-data')
    ));
    ?>

    <?php //echo $form->errorSummary($model); ?>

	<div class="row <?php echo ($form->error($model,'email') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'email'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->textField($model,'email',array('size' => 50,'maxlength' => 128, 'placeholder' => 'Email')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'email'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>
    
    
    <?php if ($model->isNewRecord) { ?>
	<div class="row <?php echo ($form->error($model,'password') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'password'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->passwordField($model,'password',array('size' => 32,'maxlength' => 32, 'placeholder' => 'Password')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'password'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>
    
    <div class="row <?php echo ($form->error($model,'password_repeat') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'password_repeat'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->passwordField($model,'password_repeat',array('size' => 32,'maxlength' => 32, 'placeholder' => 'Confirm Password')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'password_repeat'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>
    <?php } ?>
    
    <div class="row <?php echo ($form->error($model,'state_id') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'state_id'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->dropDownList($model,'state_id', CHtml::listData(State::model()->findAll(), 'id', 'name'), array('empty'=>'State')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'state_id'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>

	<div class="row <?php echo ($form->error($model,'first_name') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'first_name'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->textField($model,'first_name',array('size'=>50,'maxlength'=>64, 'placeholder' => 'First Name')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'first_name'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>

	<div class="row <?php echo ($form->error($model,'last_name') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'last_name'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->textField($model,'last_name',array('size'=>50,'maxlength'=>64, 'placeholder' => 'Last Name')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'last_name'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>

	<div class="row <?php echo ($form->error($model,'phone') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'phone'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->textField($model,'phone',array('size'=>32,'maxlength'=>32, 'placeholder' => 'Phone')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'phone'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>

	<div class="row <?php echo ($form->error($model,'company_name') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'company_name'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->textField($model,'company_name',array('size'=>50,'maxlength'=>64, 'placeholder' => 'Company Name')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'company_name'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>

	<div class="row <?php echo ($form->error($model,'newsletter') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'newsletter'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->dropDownList($model,'newsletter', array('1' => 'Yes', '2' => 'No')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'newsletter'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>

	<div class="row <?php echo ($form->error($model,'address') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'address'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->textField($model,'address', array('size'=>50,'maxlength'=>100, 'placeholder' => 'Address')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'address'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>

	<div class="row <?php echo ($form->error($model,'address_2') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'address_2'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->textField($model,'address_2', array('size'=>50,'maxlength'=>100, 'placeholder' => 'Address 2')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'address_2'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>

	<div class="row <?php echo ($form->error($model,'post_code') ? 'error' : '') ?>">
        <div class="field-left">
            <?php echo $form->labelEx($model,'post_code'); ?>
        </div>
        <div class="text_error">
            <div class="field-right">
                <?php echo $form->textField($model,'post_code', array('placeholder' => 'Post Code')); ?>
            </div>
            <div class="field-error">
                <?php echo $form->error($model,'post_code'); ?>
            </div>
        </div>
        <div class="clear"></div>
	</div>

	<div class="row button">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Update', array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->