<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $this->pageTitle; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/theme.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/external-pages.css">
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
        <!-- scripting -->
        <script src="<?php echo Yii::app()->baseUrl; ?>/js/theme.js"></script>
    </head>
    <body>
        <!--scrolltop-->
        <a href="#" class="scrolltop"><span>up</span></a>

        <!-- begins navbar -->
        <?php $this->renderPartial('//layouts/_header') ?>
        <!-- ends navbar -->

        <?php echo $content; ?>

        <?php if (!($this->id == 'site' && $this->action->id == 'login' || ($this->id == 'user'))): ?>
            <!-- starts footer -->
            <?php $this->renderPartial('//layouts/_footer'); ?>    
            <!-- end footer -->
        <?php endif; ?>

        <?php
        $this->widget('bootstrap.widgets.TbMenu', array(
            'type'  => 'tabs',
            'items' => array(
                array(
                    'label' => "Home", 
                    "url"   => "site/index"
                )
            ),
        ));
        ?>
    </body>
</html>