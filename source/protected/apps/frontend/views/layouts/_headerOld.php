<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a><!--for responsive-->

            <a class="brand" href="<?php echo url('site/index'); ?>">
                <img src="<?php echo Yii::app()->baseUrl ?>/img/logo.png" alt="logo" />
            </a><!--logo-->

            <?php $this->renderPartial('//layouts/_menu'); ?><!--menu-->

        </div>
    </div>
</div>
