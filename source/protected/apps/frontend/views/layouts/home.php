<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description"
              content="OurKindernet is a software platform specifically designed to help kindergartens manage their communication and digital data efficiently and securel"/>
        <title>OurKindernet | Kindergartens, Day Care and Early Learning Centre Software</title>
        <link rel="stylesheet" type="text/css" href="/css/main.css"/>
        <link rel="stylesheet" type="text/css" href="/css/website.css">

            <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js' type="text/javascript"></script>
            <script src='/js/jquery.mousewheel.js' type="text/javascript"></script>
            <script src='/js/class.horinaja.jquery.js' type='text/javascript'></script>
            <link rel="stylesheet" href="/css/horinaja.css" type="text/css" media="screen"/>

            <script> $(document).ready(function() {
                    $('#splash_content').Horinaja({
                        capture: 'splash_content', delai: 1,
                        duree: 20, pagination: true});
                });
            </script>

    </head>

    <body>
        <div id="header_wrap">
            <header>
                <div id="login">
                    <a href="<?php echo url('/user/login'); ?>">Log in</a> <a href="<?php echo url('/site/signup'); ?>">Sign Up</a></div>
                <img src="/img/logo.jpg" id="logo"/>
                <nav class="horizontal">
                    <ul id="yw0">
                        <li style="border-left:none;padding-left:none" class="current"><a
                                href="<?php echo url('/site/index'); ?>">Home</a></li>
                        <li><a href="<?php echo url('/site/contact'); ?>">Contact Us</a></li>
                    </ul>
                </nav>
            </header>
        </div>
        <div id="wrap">
            <?php if (app()->controller->id == 'site' && app()->controller->action->id == 'index') { ?>
                <div id="splash">
                    <div id="splash_content" class="horinaja">
                        <ul>
                            <li>
                                <img src="/img/index1.jpg"/>
                                <div>
                                    <h1>OurKindernet is a software platform specifically designed to help kindergartens manage their
                                        communication and digital data efficiently and securely</h1>
                                    <p><span>Today more than ever it is imperative to have secure communication and digital data storage for any group or organisation.</span>
                                    </p>
                                </div>
                            </li>
                            <li>
                                <img src="/img/index2.png"/>
                                <div>
                                    <h1>With OurKindernet keeping parents informed on what is going on at your kindergarten has
                                        never been easier</h1>
                                    <p><span>Parents need to be keep up-to-date with what is going on at the kinder and today - with electornic media and OurKindernet - you can easily inform all parent or/and organisations of what is happening at the kinder.</span>
                                    </p>
                                </div>
                            </li>
                            <li>
                                <img src="/img/index3.jpg"/>
                                <div>
                                    <h1>Keeping your digital data secure is imperative in today's day and age</h1>
                                    <p><span>Our kindernet is a secure online application to help you safely store you documents, records, contacts and student information, allowing the administrator to nominate who has access</span>
                                    </p>
                                </div>
                            </li>
                            <li>
                                <img src="/img/index4.jpg"/>

                                <div>
                                    <h1>Account keeping has now been made easy with OurKindernets accounting system</h1>

                                    <p><span>Keep all your expenses in an easy to use system allowing you to download and pass over to your accountant.</span>
                                    </p>
                                </div>
                            </li>
                        </ul>
                        <br style="clear:both"/>
                    </div>
                </div>
            <?php } ?>
            <div id="content">
                <?php echo $content ?>
            </div>
        </div>

        <footer>
            <div id="footer">
                <a href="/site/terms">Terms of Service</a>

                <div id="copyright">&copy; OurKindernet 2013</div>
                </br>
                <ul>
                    <li>OurKindernet</li>
                    <li>ABN: 43 946 853 315</li>
                    <li>Phone: (03) 9005 8537</li>
                </ul>
            </div>
        </footer>

    </body>
</html>