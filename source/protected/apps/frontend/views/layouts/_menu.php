<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">

            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a><!--for responsive-->

            <a class="brand" href="<?php echo url('site/index') ?>">
                <img src="<?php echo Yii::app()->baseUrl ?>/img/logo.png" alt="logo" />
            </a><!--logo-->

            <div class="nav-collapse collapse">
                <ul class="nav pull-right">
                    <?php if ($this->id == 'site' && $this->action->id == 'index') : ?>
                        <li><a href="#" class="scroller" data-section="#features">Features</a></li>
                        <li><a href="#" class="scroller" data-section="#pricing">Pricing</a></li>
                        <li><a href="#" class="scroller" data-section="#footer">Contact us</a></li>
                    <?php else: ?>
                        <li><a href="<?php echo url('site/page', array('view' => 'features')) ?>">Features</a></li>
                        <li><a href="<?php echo url('site/page', array('view' => 'pricing')) ?>">Pricing</a></li>
                        <li><a href="<?php echo url('site/contact') ?>">Contact us</a></li>
                    <?php endif; ?>
                    <li><a href="<?php echo url('site/page', array('view' => 'blog')) ?>">Blog</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            External Pages<b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo url('site/page', array('view' => 'features')) ?>">Features</a></li>
                            <li><a href="<?php echo url('site/page', array('view' => 'pricing')) ?>">Pricing</a></li>
                            <li><a href="<?php echo url('site/page', array('view' => 'portfolio')) ?>">Portfolio</a></li>
                            <li><a href="<?php echo url('site/page', array('view' => 'coming_soon')) ?>">Coming Soon</a></li>
                            <li><a href="<?php echo url('site/page', array('view' => 'about')) ?>">About us</a></li>
                            <li><a href="<?php echo url('site/contact') ?>">Contact us</a></li>
                            <li><a href="<?php echo url('site/page', array('view' => 'faq')) ?>">FAQ</a></li>
                        </ul>
                    </li>
                    <?php if (Yii::app()->user->isGuest) : ?>
                        <li>
                            <a class="btn-header" href="<?php echo url('user/register') ?>">Sign up</a>
                        </li>
                        <li>
                            <a class="btn-header" href="<?php echo url('site/login') ?>">Sign in</a>
                        </li>
                    <?php else : ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                My Profile<b class="caret"></b>
                            </a>

                            <ul class="dropdown-menu">
                                <li><a href="<?php echo url('user/profile'); ?>">View Profile</a></li>
                                <li><a href="<?php echo url('user/changePassword'); ?>">Change Password</a></li>
                                <li><a href="<?php echo url('site/logout'); ?>">Logout</a></li>
                            </ul><!--sub menu-->

                        </li><!--profile-->
                    <?php endif; ?>
                </ul>
            </div><!--menu-->
        </div>
    </div>
</div>