<?php

$controllerId = app()->controller->id;
$actionId     = app()->controller->action->id;

$insideIndexSiteItems = array(
    array(
        'label'       => 'Features',
        'url'         => '#',
        'linkOptions' => array(
            'class'        => 'scroller',
            'data-section' => '#features',
        ),
    ),
    // Pricing
    array(
        'label'       => 'Pricing',
        'url'         => '#',
        'linkOptions' => array(
            'class'        => 'scroller',
            'data-section' => '#pricing',
        )
    ),
    // Contact us
    array(
        'label'       => 'Contact us',
        'url'         => '#',
        'linkOptions' => array(
            'class'        => 'scroller',
            'data-section' => '#footer',
        )
    )
);

$externalMenu = array(
    // Features
    array(
        'label'  => 'Features',
        'url'    => url('site/page', array('view' => 'features')),
        'active' => app()->request->getParam('view') == 'features',
    ),
    // pricing
    array(
        'label'  => 'Pricing',
        'url'    => url('site/page', array('view' => 'pricing')),
        'active' => app()->request->getParam('view') == 'pricing',
    ),
    // Contact
    array(
        'label'  => 'Contact',
        'url'    => url('site/contact'),
        'active' => $actionId == 'contact',
    ),
);

$mainMenu = array(
    // Blog
    array(
        'label'  => 'Blog',
        'url'    => url('site/page', array('view' => 'blog')),
        'active' => app()->request->getParam('view') == 'blog',
    ),
    array(
        'label'       => 'External Pages',
        'url'         => '#',
        'linkOptions' => array(
            'class'       => 'dropdown-toggle',
            'data-toggle' => 'dropdown'
        ),
        'items'       => array(
            array(
                'label'  => 'Features',
                'url'    => url('site/page', array('view' => 'features')),
                'active' => app()->request->getParam('view') == 'features',
            ),
            array(
                'label'  => 'Pricing',
                'url'    => url('site/page', array('view' => 'pricing')),
                'active' => app()->request->getParam('view') == 'pricing',
            ),
            array(
                'label'  => 'Portfolio',
                'url'    => url('site/portfolio'),
                'active' => $actionId == 'portfolio',
            ),
            array(
                'label'  => 'Coming soon',
                'url'    => url('site/coming_soon'),
                'active' => $actionId == 'coming_soon',
            ),
            array(
                'label'  => 'About us',
                'url'    => url('site/about'),
                'active' => $actionId == 'about',
            ),
            array(
                'label'  => 'Contact',
                'url'    => url('site/contact'),
                'active' => $actionId == 'contact',
            ),
            array(
                'label'  => 'FAQ',
                'url'    => url('site/faq'),
                'active' => $actionId == 'faq',
            ),
        )
    ), // External page
    array(
        'label'       => 'Sign up',
        'url'         => url('user/register'),
        'active'      => $controllerId == 'user' && $actionId == 'register',
        'linkOptions' => array(
            'class' => 'btn-header',
        ),
        'visible'     => app()->user->isGuest,
    ), //Sign up
    array(
        'label'       => 'Sign in',
        'url'         => url('site/login'),
        'active'      => $controllerId == 'site' && $actionId == 'login',
        'linkOptions' => array(
            'class' => 'btn-header',
        ),
        'visible'     => app()->user->isGuest,
    ), //Sign up
    array(
        'label'   => 'My Profile',
        'url'     => '#',
        'visible' => !app()->user->isGuest,
        'items'   => array(
            array(
                'label'  => 'View Profile',
                'url'    => url('user/profile'),
                'active' => $controllerId == 'user' && $actionId == 'profile',
            ),
            array(
                'label'  => 'Change Password',
                'url'    => url('user/changePassword'),
                'active' => $controllerId == 'user' && $actionId == 'changePassword',
            ),
            '---',
            array(
                'label' => 'Logout',
                'url'   => url('site/logout'),
            ),
        )
    ), // Profile
);

$menuItems = ($controllerId == 'site' && $actionId == 'index') ?
    array_merge($insideIndexSiteItems, $mainMenu) 
    :
    array_merge($externalMenu, $mainMenu);


$logoPath = app()->baseUrl . '/img/logo.png';
$logo     = "<img src=" . $logoPath . " alt='logo' />";

$this->widget('bootstrap.widgets.TbNavbar', array(
    'type'     => null, // null or 'inverse'
    'fixed'    => 'top',
    'brand'    => $logo,
    'brandUrl' => url('site/index'),
    'collapse' => true, // requires bootstrap-responsive.css
    'items'    => array(
        array(
            'class'       => 'bootstrap.widgets.TbMenu',
            'htmlOptions' => array('class' => 'pull-right'),
            'items'       => $menuItems,
        ),
    ),
));



