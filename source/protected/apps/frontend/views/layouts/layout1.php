<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
    <div id="hero">
        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block'     => true, // display a larger alert block?
            'fade'      => true, // use transitions?
            'closeText' => 'x', // close link text - if set to false, no close link is displayed
            'alerts'    => array(// configurations per alert type
                'success' => array(
                    'block'     => true, 
                    'fade'      => true, 
                ), // success, info, warning, error or danger
            ),
        ));
        ?>
        <!--flash alert-->
        <?php
        if (isset($this->breadCrumbs)) {
            $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                'links' => $this->breadCrumbs,
            ));
        }
        ?><!--breadcrumbs-->

        <?php echo $content; ?><!-- content -->
    </div>
</div>
<?php $this->endContent(); ?>