<?php

class UserController extends Controller
{
    //public $layout = 'layout1';
    
    public function actionRegister()
    {
        $model = new User('register');

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->activation_key = Utils::md5(time());
            $model->status = User::STATUS_DEACTIVE;
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Create sucessful. Please active account form your email to completed create account!');
                // Send email verify
                UserHelper::sendEmailNewAcountUser($model->id);
                $this->redirect(array('register'));
            }
        }

        $this->render('register', array('model' => $model));
    }

    public function actionVerify()
    {
        $verifyCode = Yii::app()->request->getParam('vr', null);
        if (!empty($verifyCode)) {
            $user = User::model()->find('activation_key = "' . $verifyCode . '"');
            if (!empty($user)) {
                $user->status = User::STATUS_ACTIVE;
                $user->activation_key = NULL;
                if ($user->save()) {
                    Yii::app()->user->setFlash('success', 'Active account sucessful. You can sign in here!');
                    $this->redirect(array('site/login'));
                }
            } else {
                // redirect page error
            }
        }
    }

    public function actionProfile()
    {
        $this->layout = 'layout1';
        $this->pageTitle= app()->name . ' - My Profile';
        $this->breadCrumbs = array('Profile');
        
        $model = User::model()->findByPk(user()->id);
        
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Update sucessful !');
                $this->redirect(array('profile'));
            }
        }
        $this->render('profile', array('model' => $model));
    }

    public function actionChangePassword()
    {
        $this->layout      = 'layout1';
        $this->pageTitle   = app()->name . ' - Change Password';
        $this->breadCrumbs = array('Profile'=> 'profile', 'Change Password');

        $model = new ChangePasswordForm();
        $user = User::model()->findByPk(user()->id);

        if (isset($_POST["ChangePasswordForm"])) {
            $model->attributes = $_POST["ChangePasswordForm"];
            if ($model->validate()) {
                if (Utils::md5($model->oldPassword) != $user->password) {
                    $model->addError('oldPassword', 'Your password is incorrect');
                } else {
                    $user->password = Utils::md5($model->newPassword);
                    $user->save();
                    Yii::app()->user->setFlash("success", "Your password is changed");
                    $this->redirect(array('user/changePassword'));
                }
            }
        }
        $this->render('changePassword', array('model' => $model));
    }

}
