<?php

class SiteController extends Controller
{
    public function actions()
    {
        return array(
            'error' => 'common.actions.ErrorAction',
            'page' => array(
				'class' => 'CViewAction',
			),
        );
    }

    public function actionIndex()
    {
        $this->render('index');
    }
    
    public function actionContact()
    {
        $this->render('contact');
    }

    public function actionTerms()
    {
        cs()->registerMetaTag("OurKindernet terms service conditions include Provision, Proprietary Rights, Submitted Content, Termination of Agreement, Disclaimer of Warranties, Limitation of Liability, External Content, Jurisdiction, Entire Agreement, Changes to the Terms", "description");
        $this->render('terms');
    }

    public function actionLogin()
    {
        if (user()->isGuest) {
            $model = new LoginForm;
            $message = "";

            // collect user input data
            if (isset($_POST['LoginForm'])) {
                $model->attributes = $_POST['LoginForm'];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login()) {
                    $this->redirect(Yii::app()->user->returnUrl);
                } else {
                    if (count($model->errors) > 0) {
                        foreach ($model->errors as $key => $error) {
                            $message .= $error[0] . '<br/>';
                        }
                    }
                }
            } 

            // display the login form
            $this->render('login',array('model'=>$model, 'message' => $message));
        } else {
            $this->redirect(Yii::app()->user->returnUrl);
        }
    }
    
    public function actionForgotPassword()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $email = Yii::app()->request->getParam('email');
            $user = User::model()->findByAttributes(array('email' => $email, 'status' => User::STATUS_ACTIVE));
            if ($user == null) {
                $message = "Sorry, but we can not find this email address in our records. You can try another email address, or contact us for help.";
                echo "{success : false, message : '" . $message . "'}";
                Yii::app()->end();
            } else {
                $user->passwordResetString  = md5(time());
                $resetTimeout               = time() + param('resetForGotPasswordTimeout');
                $user->passwordResetTimeout = date('Y-m-d H:i:s', $resetTimeout);
                $user->save(false);
                
                // Send email forgot password
                $res = UserHelper::sendEmailSendForgotPassword($user);
                
                if ($res)
                    echo "{success :  true, message : 'An email explaining how to reset your password has been sent to you'}";
            }
        } else {
            $model = new ForgotPasswordForm();
            $fg = Yii::app()->request->getParam('fg');
            $user = User::model()->findByAttributes(array('passwordResetString' => $fg));
            if (!empty($user)) {
                if (strtotime($user->passwordResetTimeout) < strtotime(date('Y-m-d H:i:s'))) {
                    Yii::app()->user->setFlash('success', 'Expired session. You need request new forgot password!');
                }

                if (isset($_POST['ForgotPasswordForm'])) {
                    $model->attributes = $_POST['ForgotPasswordForm'];
                    if ($model->validate()) {
                        $user->password = md5($model->password);
                        if ($user->save()) {
                            // Send email for user shange password successful
                            UserHelper::sendEmailChangePassword($user);
                            Yii::app()->user->setFlash('success', 'Password is update sucessful. You can sign in here!');
                            $this->redirect(url('site/login'));
                        }
                    }
                }
                
                $this->render('forgotPassword', array('model'=>$model));
                
            } else {
                throw new CHttpException(403, "You aren't allowed to access to this action.");
            }
        }
    }

    public function actionRestorePassword()
    {
        
    }

    public function actionLogout()
    {
        user()->logout(true);
        $this->redirect(app()->baseUrl);
    }

    public function actionMessage()
    {
        $this->render('message');
    }

}