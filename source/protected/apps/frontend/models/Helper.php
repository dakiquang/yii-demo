<?php
class Helper
{
    public static function getLeftMenu()
    {
        return array(
            array('visible' => user()->checkAccess('setting'),'label' => 'General Setting', 'icon' => 'icon-chevron-right', 'url' => url('setting'), 'active' => app()->controller->id == 'setting'),
            array('visible' => user()->checkAccess('user') ,'label' => 'User Management', 'icon' => 'icon-chevron-right', 'url' => url('user'), 'active' => app()->controller->id == 'user'),
            array('visible' => user()->checkAccess('emailTemplate'), 'label' => 'Email Template', 'icon' => 'icon-chevron-right', 'url' => url('emailTemplate'), 'active' => app()->controller->id == 'emailTemplate'),
        );
    }
}