<?php
class ForgotPasswordForm extends CFormModel {
	public $password;
	public $rePassword;
	
	
	public function rules() {
		return array(
			array('password, rePassword', 'required'),
			array('password, rePassword', 'length', 'min' => param('passwordLMin')),
			array('rePassword', 'compare', 'compareAttribute' => 'password'),
		);
	}
    
    public function attributeLabels() {
		return array(
			'password' => 'Password',
			'rePassword' => 'Re password'
		);
	}
}
