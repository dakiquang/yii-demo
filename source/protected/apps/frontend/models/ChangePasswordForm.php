<?php
class ChangePasswordForm extends CFormModel {
	public $oldPassword;
	public $newPassword;
	public $repeatNewPassword;

	public function rules() {
		return array(
			array('oldPassword, newPassword,repeatNewPassword', 'required'),

			array('newPassword', 'length', 'min' => param('passwordLMin')),
			array('repeatNewPassword', 'compare', 'compareAttribute' => 'newPassword'),
		);
	}

	public function attributeLabels() {
		return array(
			'oldPassword' => 'Old password',
			'newPassword' => 'New password',
			'repeatNewPassword' => 'Re-new password'
		);
	}
}
