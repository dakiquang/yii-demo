<?php

class RestorePasswordForm extends CFormModel
{
    public $email;

    public function rules()
    {
        return array(
            array('email', 'required'),
            array('email', 'email'),
            array('email', 'email')
        );
    }

    public function checkEmail ()
    {
        $user = User::model()->findByAttributes(array('email' => $this->email));
        if($user == null)
            $this->addError('email', 'Email not found.');
    }

}