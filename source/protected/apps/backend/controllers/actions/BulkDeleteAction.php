<?php

class BulkDeleteAction extends BulkAction
{
	public function run()
	{
		if (app()->request->isPostRequest) {
			// we only allow deletion via POST request		
			$models = $this->controller->getModel(null)->findAllByPk(self::getSelectedValues());
			if (!empty($models) && is_array($models)) {
				foreach ($models as $model)
					$model->delete();
			}
			
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->controller->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			else app()->end();
		}
	}
}