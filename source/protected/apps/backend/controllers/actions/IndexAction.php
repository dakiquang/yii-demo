<?php

class IndexAction extends CAction
{
	public function run()
	{
    	$model = $this->controller->getModel('search');
    	$model->unsetAttributes();
    	if (isset($_GET[$this->controller->modelClass])) {
    		$model->attributes = $_GET[$this->controller->modelClass];
    	}
		$this->controller->render('index', array('model' => $model));
	}
}