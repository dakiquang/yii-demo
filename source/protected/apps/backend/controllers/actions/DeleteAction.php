<?php

class DeleteAction extends CAction
{
	public function run($id)
	{
		if (app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->controller->getModel(null)->findByPk($id)->delete();
						
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->controller->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			else app()->end();
		}
	}
}