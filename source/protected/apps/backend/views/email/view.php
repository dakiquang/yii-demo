<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View Email #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update Email',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('email/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'type',
		'sender_email',
		'sender_name',
		'recipient_email',
		'recipient_name',
		'cc',
		'bcc',
		'subject',
		'content',
		'is_sent',
		'sent_time',
		'created_time',
	),
)); ?>