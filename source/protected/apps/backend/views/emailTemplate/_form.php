<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'emailTemplate-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> EmailTemplate</legend>
	<?php
	echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 128));
	echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8'));
	echo $form->textFieldRow($model, 'sender_name', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textFieldRow($model, 'sender_email', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textFieldRow($model, 'receiver_name', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textFieldRow($model, 'receiver_email', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textAreaRow($model, 'subject', array('rows' => 6, 'cols' => 50, 'class' => 'span8'));
	echo $form->redactorRow($model, 'body', array('class' => 'span4', 'rows' => 5));;
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('emailTemplate/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>