<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View EmailTemplate #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update EmailTemplate',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('emailTemplate/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'name',
		'description',
		'sender_name',
		'sender_email',
		'receiver_name',
		'receiver_email',
		'subject',
		'body',
	),
)); ?>