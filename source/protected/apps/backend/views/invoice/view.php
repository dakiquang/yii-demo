<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View Invoice #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update Invoice',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('invoice/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'auction_id',
		'amount',
		'pay_time',
		'created_time',
	),
)); ?>