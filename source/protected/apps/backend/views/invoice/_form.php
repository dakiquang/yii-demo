<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'invoice-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> Invoice</legend>
	<?php
	echo $form->textFieldRow($model, 'auction_id', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'amount', array('class' => 'span5', 'maxlength' => 10));
	echo $form->textFieldRow($model, 'pay_time', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'created_time', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('invoice/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>