<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View Bid #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update Bid',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('bid/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'auction_id',
		'bidder_id',
		'amount',
		'updated_time',
		'created_time',
	),
)); ?>