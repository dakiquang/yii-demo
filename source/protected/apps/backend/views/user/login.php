<!DOCTYPE html>
<html lang="en">
<head>    
    <title><?php echo app()->name ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="/backend/css/login.css" rel="stylesheet" />
</head>
<body>
	<div class="container">
	<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'type' => 'horizontal',
	));
	?>
	<h2 class="login-form-heading">Please log in</h2>
	<?php $this->renderPartial('//layouts/partial/notification') ?>
	<?php echo GHtml::activeTextField($model, 'username', array('class' => 'input-block-level', 'placeholder' => 'Username')); ?>
	<?php echo GHtml::activePasswordField($model, 'password', array('class' => 'input-block-level', 'placeholder' => 'Password')); ?>
	<?php
	$this->widget('bootstrap.widgets.TbButton', array(
		'label' => "Log in",
		'buttonType' => 'submit',
		'type' => 'primary',
		'size' => 'large',
	));
	?>
	<?php $this->endWidget(); ?>
	</div>
</body>
</html>