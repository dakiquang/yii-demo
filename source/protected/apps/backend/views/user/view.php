<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View User #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update User',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('user/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'theme_id',
		'email',
		'password',
		'first_name',
		'last_name',
		'phone',
		'company_name',
		'subdomain',
		'newsletter',
		'logo',
		'status',
		'activation_key',
		'created_time',
		'updated_time',
	),
)); ?>