<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'user-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> User</legend>
	<?php
	echo $form->textFieldRow($model, 'theme_id', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 128));
	echo $form->passwordFieldRow($model, 'password', array('class' => 'span5', 'maxlength' => 32));
	echo $form->textFieldRow($model, 'first_name', array('class' => 'span5', 'maxlength' => 64));
	echo $form->textFieldRow($model, 'last_name', array('class' => 'span5', 'maxlength' => 64));
	echo $form->textFieldRow($model, 'phone', array('class' => 'span5', 'maxlength' => 32));
	echo $form->textFieldRow($model, 'company_name', array('class' => 'span5', 'maxlength' => 64));
	echo $form->textFieldRow($model, 'subdomain', array('class' => 'span5', 'maxlength' => 50));
	echo $form->textFieldRow($model, 'newsletter', array('class' => 'span5'));
	echo $form->redactorRow($model, 'logo', array('class' => 'span4', 'rows' => 5));;
	echo $form->textFieldRow($model, 'status', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'activation_key', array('class' => 'span5', 'maxlength' => 128));
	echo $form->textFieldRow($model, 'created_time', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'updated_time', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('user/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>