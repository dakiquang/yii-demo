<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'newsletterEmail-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> NewsletterEmail</legend>
	<?php
	echo $form->textFieldRow($model, 'newsletter_id', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'type', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'sender_email', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textFieldRow($model, 'sender_name', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textFieldRow($model, 'recipient_email', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textFieldRow($model, 'recipient_name', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textFieldRow($model, 'cc', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textFieldRow($model, 'bcc', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textAreaRow($model, 'subject', array('rows' => 6, 'cols' => 50, 'class' => 'span8'));
	echo $form->redactorRow($model, 'content', array('class' => 'span4', 'rows' => 5));;
	echo $form->textFieldRow($model, 'is_sent', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'sent_time', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'created_time', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('newsletterEmail/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>