<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'state-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> State</legend>
	<?php
	echo $form->textFieldRow($model, 'id', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 20));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('state/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>