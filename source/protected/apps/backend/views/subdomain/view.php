<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View Subdomain #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update Subdomain',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('subdomain/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'user_id',
		'name',
		'address',
		'address_2',
		'company_name',
		'header_background',
		'logo',
		'created_time',
		'updated_time',
	),
)); ?>