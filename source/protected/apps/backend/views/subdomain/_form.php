<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'subdomain-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> Subdomain</legend>
	<?php
	echo $form->textFieldRow($model, 'user_id', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 60));
	echo $form->textFieldRow($model, 'address', array('class' => 'span5', 'maxlength' => 100));
	echo $form->textFieldRow($model, 'address_2', array('class' => 'span5', 'maxlength' => 100));
	echo $form->textFieldRow($model, 'company_name', array('class' => 'span5', 'maxlength' => 64));
	echo $form->textFieldRow($model, 'header_background', array('class' => 'span5', 'maxlength' => 20));
	echo $form->redactorRow($model, 'logo', array('class' => 'span4', 'rows' => 5));;
	echo $form->textFieldRow($model, 'created_time', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'updated_time', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('subdomain/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>