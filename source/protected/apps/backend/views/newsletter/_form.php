<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'newsletter-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> Newsletter</legend>
	<?php
	echo $form->textFieldRow($model, 'subject', array('class' => 'span5', 'maxlength' => 255));
	echo $form->redactorRow($model, 'content', array('class' => 'span4', 'rows' => 5));;
	echo $form->textFieldRow($model, 'created_time', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'updated_time', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('newsletter/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>