<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View CreditHistory #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update CreditHistory',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('creditHistory/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'user_id',
		'number_credit',
		'price',
		'amount',
		'invoice',
		'pay_time',
	),
)); ?>