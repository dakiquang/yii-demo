<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'creditHistory-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> CreditHistory</legend>
	<?php
	echo $form->textFieldRow($model, 'user_id', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'number_credit', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'price', array('class' => 'span5', 'maxlength' => 10));
	echo $form->textFieldRow($model, 'amount', array('class' => 'span5', 'maxlength' => 10));
	echo $form->textFieldRow($model, 'invoice', array('class' => 'span5', 'maxlength' => 16));
	echo $form->textFieldRow($model, 'pay_time', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('creditHistory/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>