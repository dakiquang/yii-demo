<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'product-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> Product</legend>
	<?php
	echo $form->textFieldRow($model, 'catagory_id', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'created_by', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 64));
	echo $form->redactorRow($model, 'description', array('class' => 'span4', 'rows' => 5));;
	echo $form->redactorRow($model, 'photo', array('class' => 'span4', 'rows' => 5));;
	echo $form->textFieldRow($model, 'created_time', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('product/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>