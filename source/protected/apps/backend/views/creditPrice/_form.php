<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'creditPrice-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> CreditPrice</legend>
	<?php
	echo $form->textFieldRow($model, 'price', array('class' => 'span5', 'maxlength' => 10));
	echo $form->textFieldRow($model, 'valid', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('creditPrice/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>