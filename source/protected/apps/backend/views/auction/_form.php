<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'auction-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> Auction</legend>
	<?php
	echo $form->textFieldRow($model, 'product_id', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'created_by', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'auction_type', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'starting_price', array('class' => 'span5', 'maxlength' => 10));
	echo $form->textFieldRow($model, 'current_price', array('class' => 'span5', 'maxlength' => 10));
	echo $form->textFieldRow($model, 'final_price', array('class' => 'span5', 'maxlength' => 10));
	echo $form->textFieldRow($model, 'lastest_bidder_id', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'expired_time', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'created_time', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('auction/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>