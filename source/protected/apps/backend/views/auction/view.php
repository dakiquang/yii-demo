<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View Auction #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update Auction',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('auction/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'product_id',
		'created_by',
		'auction_type',
		'starting_price',
		'current_price',
		'final_price',
		'lastest_bidder_id',
		'expired_time',
		'created_time',
	),
)); ?>