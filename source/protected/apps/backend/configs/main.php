<?php

return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'Our Auctions Admin',	
	'preload' => array('bootstrap'),
	'sourceLanguage' => 'en',
    'components' => array(
        'user' => array(
            'loginUrl' => 'user/login',
        ),
        'bootstrap' => array(
            'responsiveCss' => true,
        ),
    ),
	'modules' => array(
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'generatorPaths' => array(
    			'app.vendors.gii',
                'bootstrap.gii',
            ),    
			'password' => '123456',
			'ipFilters' => array('127.0.0.1', '::1'),
		),
	),
	'params' => require(dirname(__FILE__) . '/params.php'),
);