<?php

class UserIdentity extends GUserIdentity 
{    
    public function authenticate()
    {
        $user = param('adminUser');
        if ($user['username'] != $this->username)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if ($user['password'] != $this->password)
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
}