<?php

class BulkAction extends CAction
{
	public static $_chkName = 'selectedValues';
	
	public static function getSelectedValues()
	{
		return (!empty($_POST) && isset($_POST[self::$_chkName])) ? $_POST[self::$_chkName] : array(); 
	}
}