<?php
$this->widget('bootstrap.widgets.TbButtonGroup', array(
	'htmlOptions' => array('class' => 'dropup'),
	'size' => 'medium',
    'buttons' => array(
		array(
			'label' => t('With selected'), 
			'items' => $items
    	),
    ),
));	