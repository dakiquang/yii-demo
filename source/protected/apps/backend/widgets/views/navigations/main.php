<?php

$this->widget('bootstrap.widgets.TbNavbar', array(
	'brand' => t(app()->name),
	'fluid' => true,
	'items' => array(
		array(
			'class' => 'bootstrap.widgets.TbMenu',
			'items' => $items[0],
		),
		array(
			'class' => 'bootstrap.widgets.TbMenu',
			'htmlOptions' => array('class'=>'pull-right'),
			'items' => $items[1],
		),
	)
));