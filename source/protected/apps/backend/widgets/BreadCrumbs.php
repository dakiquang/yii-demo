<?php

class BreadCrumbs extends CWidget
{
	public $lastTitle = '';
	
	public function run()
	{
		$this->render('bread_crumbs', array('links' => $this->buildLinks()));
	}
	
	public function buildLinks()
	{
		$links = array();
		
		$dataPath = Utils::dataViewPath('navigations');
		$menuItems = include $dataPath . '/sub.php';
		
		$c = app()->controller->id;
		foreach ($menuItems as $itemInfo) {
			if (is_array($itemInfo[1])) {
				$firstController = '';
				foreach ($itemInfo[1] as $cId => $title) {
					if (empty($firstController))
						$firstController = $cId;
					if ($c == $cId) {
						$links[t($itemInfo[0])] = url($firstController);
						$links[t($title)] = url($cId);
						break;
					}
				}
			} else {
				if ($c == $itemInfo) {
					$links[t($itemInfo[1])] = url($c);
					break;
				}
			}
		}
		
		$links[] = $this->getLastTitle();
		
		return $links;
	}
	
	public function getLastTitle()
	{		
		if (empty($this->lastTitle)) {
			$c = app()->controller;
			$a = $c->action->id;
			switch ($a) {
				case 'index':
					$a = 'list';
					break;
			}
			$this->lastTitle = (ucfirst($a) . ' ' . ucfirst($c->id));
		}
		return $this->lastTitle;
	}
}