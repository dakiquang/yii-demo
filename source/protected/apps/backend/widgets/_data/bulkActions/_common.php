<?php

/**
 * Format: 
 * 	array(
 * 		'action' 	=> 'action-name',
 * 		'title'		=> 'action-title', // Optional. If omitted, will use action-name upper-cased the first letter
 * 		'confirm' 	=> 'confirm-msg', // Optional. If set, will overwrite default confirm message
 * 		'params' 	=> array(), // Optional
 * 	),
 */

return array(
	array(
		'action' => 'delete',
	),
);