<?php

return array(
	array(
		'site/index' => 'Dashboard',
		'user/create' => 'Create user',
		'category/index' => 'Manage categories',
	),
	array(
		'site/setting' => 'Settings',
		'user/profile' => 'My profile',
		'user/logout' => 'Log out',
	),
);