<?php

return array(
    array('Credit', array(
            'creditCurrent' => 'Current',
            'creditHistory' => 'History',
            'creditPrice' => 'Credit Price'
        ),
    ),
    array('auction', 'Auctions'),
    array('product', 'Product'),
    array('emailTemplate', 'Email Templates'),
    array('newsletter', 'Newsletter'),
    array('category', 'Categories'),
    array('invoice', 'Invoices'),
    array('user', 'Users'),
);