<?php

// Define required constant
define('WWW', dirname(__FILE__));
define('APP_NAME', basename(WWW));

// Include common code
require(WWW . '/../common/index.php');