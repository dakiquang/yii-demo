<?php

// Define required constant
define('WWW', dirname(__FILE__));
define('APP_NAME', basename(WWW));

// Include shared code
require(WWW . '/../common/index.php');